import sbt._

object V {
  val scalatest = "3.0.5"
  val akka = "2.5.12"
  val akkaHttp = "10.1.5"
  val swaggerUi = "3.19.4"
  val webjarLocator = "0.34"
  val quillVersion = "2.5.4"
}

object Dependencies {

  lazy val scalatest = "org.scalatest" %% "scalatest" % V.scalatest % Test
  lazy val akkaHttp = "com.typesafe.akka" %% "akka-http" % V.akkaHttp
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % V.akka
  lazy val sprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % V.akkaHttp
  lazy val swaggerUi =   "org.webjars.npm" % "swagger-ui-dist" % V.swaggerUi
  lazy val webjarLocator = "org.webjars" % "webjars-locator-core" % V.webjarLocator
  lazy val akkaHttpTestkit = "com.typesafe.akka" %% "akka-http-testkit" % V.akkaHttp % Test

  lazy val telegram = Seq("com.bot4s" %% "telegram-core" % "4.0.0-RC2", "com.bot4s" %% "telegram-akka" % "4.0.0-RC2")

  lazy val quill = Seq("com.h2database" % "h2" % "1.4.192", "io.getquill" %% "quill-jdbc" % "2.6.0")

  lazy val akkaHttpStack =
    Seq(akkaHttp,
      akkaStream,
      sprayJson,
      swaggerUi,
      webjarLocator,
      akkaHttpTestkit,
      scalatest)

  lazy val logging = Seq("org.slf4j" % "slf4j-api" % "1.7.5", "org.slf4j" % "slf4j-simple" % "1.7.5")
}
