package ru.tinkoff.chat

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import spray.json._

import scala.concurrent.Future

object Utils {
  class Weather(city: String)(implicit val as: ActorSystem) {
    val apiKey = "51e952104c47982a3b1b943e4b360fdf"
    val url = s"http://api.openweathermap.org/data/2.5/weather?q=$city&units=metric&APPID=$apiKey"
    val responseFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = url))

    def getWeather(implicit mat: Materializer): Future[String] = {
      import as.dispatcher
      for {
        response <- responseFuture
        jsObject <- Unmarshal(response.entity).to[String].map(_.parseJson.asJsObject)
      } yield  parseJsObject(jsObject)
    }

    def parseJsObject(obj: JsObject): String = {
      obj.getFields("main").headOption match {
        case Some(value) => s"Current temperature is ${value.asJsObject.getFields("temp").head.toString()}\n"
        case None => "no such city\n"
      }
    }
  }

  class MyPeriod(hours: Long, minutes: Long, seconds: Long) {
    val secondsDuration: Long = 3600 * hours + 60 * minutes + seconds

    val render: String = {
      {
        if (hours != 0) s"hours: $hours " else ""
      } + {
        if (minutes != 0) s"minutes: $minutes " else ""
      } + {
        if (seconds != 0) s"seconds: $seconds " else ""
      }
    }
  }

  object MyPeriod {
    def apply(timestamp1: LocalDateTime, timestamp2: LocalDateTime): MyPeriod =  {
      val hours = timestamp1.until(timestamp2, ChronoUnit.HOURS)
      val minutes = timestamp1.plusHours(hours).until(timestamp2, ChronoUnit.MINUTES)
      val seconds = timestamp1.plusHours(hours).plusMinutes(minutes).until(timestamp2, ChronoUnit.SECONDS)
      new MyPeriod(hours, minutes, seconds)
    }
  }

  object Implicits {
    implicit class LocalDateTimeCompare(val ld: LocalDateTime)
      extends Ordered[LocalDateTime] {
      def compare(that: LocalDateTime): Int = ld.compareTo(that)
    }

    implicit class MyPeriodCompare(val period: MyPeriod) extends Ordered[MyPeriod] {
      override def compare(that: MyPeriod): Int = period.secondsDuration.compareTo(that.secondsDuration)
    }
  }
}
