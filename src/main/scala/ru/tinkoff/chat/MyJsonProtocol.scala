package ru.tinkoff.chat

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import ru.tinkoff.chat.connection.MyCredentials
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

object MyJsonProtocol extends DefaultJsonProtocol with SprayJsonSupport{
  implicit val credentialsJsonFormat: RootJsonFormat[MyCredentials] = jsonFormat2(MyCredentials)
  implicit val messageJsonFromat: RootJsonFormat[ChatMessage] = jsonFormat2(ChatMessage)
}
