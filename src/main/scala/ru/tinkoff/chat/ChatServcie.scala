package ru.tinkoff.chat

import java.math.BigInteger
import java.security.MessageDigest
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.{ActorMaterializer, Materializer}
import ru.tinkoff.chat.Utils.{MyPeriod, Weather}
import ru.tinkoff.chat.entity.Provider.Provider
import ru.tinkoff.chat.entity.{Message, Provider, User, UserAuth}
import ru.tinkoff.chat.repository.{MessageRepository, UserAuthRepository, UserRepository}

import scala.collection.mutable
import scala.concurrent.Future
import scala.util.matching.Regex

class ChatServcie(dbContext: MyContext) {
  val activeUsers: mutable.HashMap[String, ActorRef] = mutable.HashMap.empty
  val userRepo = new UserRepository(dbContext)
  val userAuthRepo = new UserAuthRepository(dbContext)
  val messageRepo = new MessageRepository(dbContext)
  val messages: mutable.Buffer[Message] = mutable.Buffer.empty
  val recentCount = 15
  val weatherCommand: Regex = "weather ([a-zA-Z_]+)".r

  import Utils.Implicits._

  def login(userName: String, userId: Option[Long], password: Option[String], myProvider: Provider): Boolean = {
    val user = userRepo.getUserByLogin(userName)

    if (user.isDefined) {
      if (password.isDefined) {
        val digest = MessageDigest.getInstance("MD5").digest(password.get.getBytes())
        val hashedPass = new BigInteger(1, digest).toString(16)

        if (user.get.password.get.equals(hashedPass) && !activeUsers.contains(userName)) {
          userRepo.update(user.get.copy(isOnline = true))
          userAuthRepo.insert(UserAuth(1, user.get.id, true, LocalDateTime.now()))
          true
        } else {
          userAuthRepo.insert(UserAuth(1, user.get.id, false, LocalDateTime.now()))
          false
        }
      } else {
        userAuthRepo.insert(UserAuth(1, user.get.id, true, LocalDateTime.now()))
        true
      }
    } else {
      register(userName, userId, password, myProvider)
      true
    }

  }



  def logout(ref: ActorRef) = {
    val userName = findOnlineUser(ref).get
    broadcastSystemMessage(ref, ChatMessage(s"$userName just logged out\n", None))
    activeUsers.remove(userName)
  }


  def register(userName: String, userId: Option[Long], password: Option[String], myProvider: Provider) = {
    val digest = MessageDigest.getInstance("MD5").digest(password.getOrElse(" ").getBytes())
    val pass = if (password.isDefined) Some(new BigInteger(1, digest).toString(16)) else None
    val user = User(0L, myProvider, userId, userName, pass, true, LocalDateTime.now())
    val localUserId = userRepo.insert(user)

    userAuthRepo.insert(UserAuth(0L, localUserId, true, LocalDateTime.now()))
  }

  def sendMessage(from: String, to: String, message: String) = {

  }

  def broadcastMessage(message: ChatMessage): Unit = {
    val user = userRepo.getUserByLogin(message.from.get)
    val users = activeUsers.filterKeys(!message.from.get.equals(_)).values
    if (user.isDefined)
      messages += Message(0L, user.get.id, message.message, LocalDateTime.now())
    users.foreach(_ ! message)
  }


  def broadcastSystemMessage(from: ActorRef, message: ChatMessage): Unit = {
    val users = activeUsers.filter(_._2 != from).values
    users.foreach(_ ! message)
  }

  def findOnlineUser(ref: ActorRef): Option[String] = {
    val user = activeUsers.filter(_._2.equals(ref))
    if (user.nonEmpty)
      Some(user.keys.head)
    else
      None
  }

  def writeMessagesInBatch()=  {
    if (messages.nonEmpty) {
      messageRepo.batchInsert(messages.seq)
      messages.clear()
    }
  }

  def getRecentMessages() = {
    writeMessagesInBatch()
    messageRepo.getMessages().sortBy(_.createdAt).takeRight(recentCount)
  }

  def getOnlineUsersSorted() = {
    val onlineUsers = activeUsers.keys.map(userRepo.getUserByLogin(_).get)
    val currentTime = LocalDateTime.now()
    val userToAuth = onlineUsers.map(user => (user, MyPeriod(getLastAuth(user).createdAt, currentTime))).toSeq
    userToAuth.sortBy(_._2).takeRight(recentCount).foldLeft("")((a, b) => a + s"${b._1.login} - ${b._2.render}\n")
  }

  def getLastAuth(user: User): UserAuth = {
    userAuthRepo.getUserAuths().filter(auth => user.id == auth.userId && auth.isSuccess).maxBy(_.createdAt)
  }

  def processCommand(command: String)(implicit as: ActorSystem, mat: Materializer): Future[String] = { command match {
      case "now" =>
        Future.successful(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "\n")
      case "who" =>
        Future.successful(getOnlineUsersSorted())
      case weatherCommand(city) =>
        new Weather(city).getWeather
      case _ =>
        Future.failed(new IllegalArgumentException("no such command"))
    }
  }

  def getMessageWithAuthor(message: Message) = {
    ChatMessage(message.text, Some(userRepo.getUserById(message.userId).get.login))
  }

  def findUser(name: String, password: String): Boolean = {
    val user = userRepo.getUserByLogin(name)
    val digest = MessageDigest.getInstance("MD5").digest(password.getBytes())
    val hashedPass = new BigInteger(1, digest).toString(16)

    if (user.isDefined && !user.get.password.get.equals(hashedPass))
      false
    else
      true
  }
}
