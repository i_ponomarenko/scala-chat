package ru.tinkoff.chat.repository

import ru.tinkoff.chat.MyContext
import ru.tinkoff.chat.entity.UserAuth

class UserAuthRepository(context: MyContext) {
  import context._
  def insert(userAuth: UserAuth): Long = {
    context.run(quote(query[UserAuth].insert(lift(userAuth)).returning(_.id)))
  }

  def getUserAuths(): List[UserAuth] = {
    context.run(quote(query[UserAuth]))
  }
}
