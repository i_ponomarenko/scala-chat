package ru.tinkoff.chat.repository

import ru.tinkoff.chat.MyContext
import ru.tinkoff.chat.entity.Message

class MessageRepository(context: MyContext) {
  import context._
  def insert(message: Message): Long = {
    context.run(quote(query[Message].insert(lift(message)).returning(_.id)))
  }

  def batchInsert(messages: Seq[Message]) = {
    context.run(quote(liftQuery(messages).foreach(query[Message].insert(_).returning(_.id))))
  }

  def getMessages(): List[Message] = {
    context.run(quote(query[Message]))
  }
}
