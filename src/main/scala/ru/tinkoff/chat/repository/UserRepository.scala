package ru.tinkoff.chat.repository

import ru.tinkoff.chat.MyContext
import ru.tinkoff.chat.entity.User

class UserRepository(context: MyContext) {
  import context._
  def getUserById(id: Long): Option[User] = {
    val q = quote {
      for {
        u <- query[User] if u.id == lift(id)
      } yield Option(u)
    }

    val user = context.run(q)
    if (user.nonEmpty)
      context.run(q).head
    else
      None
  }

  def update(user: User) = {
    implicit val userUpdateMeta = updateMeta[User](_.id)
    val q = quote(query[User].filter(_.id == lift(user.id)).update(lift(user)))

    context.run(q)
  }

  def insert(user: User): Long = {
    context.run(quote(query[User].insert(lift(user)).returning(_.id)))
  }

  def getUserByLogin(login: String): Option[User] = {
    val q = quote {
      for {
        u <- query[User] if u.login == lift(login)
      } yield Option(u)
    }

    val user = context.run(q)
    if (user.nonEmpty)
      context.run(q).head
    else
      None
  }

  def getUsers(): List[User] = {
    context.run(quote(query[User]))
  }

  def getOnlineUsers(): List[User] = {
    val q = quote {
      for {
        u <- query[User] if u.isOnline
      } yield u
    }

    context.run(q)
  }
}
