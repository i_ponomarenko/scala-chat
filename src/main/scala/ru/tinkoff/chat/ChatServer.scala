package ru.tinkoff.chat


import akka.actor.Actor
import akka.stream.ActorMaterializer
import io.getquill.{H2JdbcContext, SnakeCase}
import ru.tinkoff.chat.entity.Provider.Provider
import ru.tinkoff.chat.entity._

import scala.concurrent.Future
import scala.concurrent.duration._

abstract class Event
case class Login(userName: String, userId: Option[Long], password: Option[String], provider: Provider) extends Event
case class Logout(from: Option[String]) extends Event
case class ChatMessage(message: String, from: Option[String]) extends Event
case class LoginSuccess() extends Event
case class LoginFailure() extends Event
case class Command(command: String, from: Option[String]) extends Event
case class CommandResponse(response: Future[String]) extends Event
case class CheckPassword(userName: String, password: String) extends Event
case class CheckResponse(result: Boolean) extends Event

class MyContext(config: String) extends H2JdbcContext(SnakeCase, config) with Encoders with Decoders

class ChatServer extends Actor {
  val ctx = new MyContext("ctx")
  val chatService = new ChatServcie(ctx)
  implicit val s = context.system
  implicit val materializer = ActorMaterializer()
  import s.dispatcher
  context.system.scheduler.schedule(0 seconds, 10 seconds) {
    chatService.writeMessagesInBatch()
  }


  override def receive: Receive = {
    case Login(userName, userId, password, myProvider) =>
      println("login" + sender())
      if (chatService.login(userName, userId, password, myProvider)) {
        println(s"$userName just logged in")
        sender ! LoginSuccess
        chatService.getRecentMessages().foreach(m => sender ! chatService.getMessageWithAuthor(m))
        chatService.broadcastSystemMessage(sender, ChatMessage(s"$userName just logged in!\n", None))
        chatService.activeUsers.put(userName, sender)
      } else {
        println(s"$userName failed to log in")
        sender ! LoginFailure
      }
    case Logout(from) =>
      val name = from.getOrElse(chatService.findOnlineUser(sender).get)
      println(s"$name logged out")
      chatService.logout(chatService.activeUsers(name))
    case m: ChatMessage =>
      println("msg" + sender())
      val fromName = m.from.getOrElse(chatService.findOnlineUser(sender).get)
      println(s"recieved msg from $fromName")
      chatService.broadcastMessage(m.copy(from = Some(fromName)))
    case Command(command, from) =>
      val response = CommandResponse(chatService.processCommand(command))
      if (from.isEmpty)
        sender ! response
      else
        chatService.activeUsers(from.get) ! response
    case CheckPassword(name, pass) =>
      sender ! CheckResponse(chatService.findUser(name, pass))
  }
}
