package ru.tinkoff.chat.entity

import io.getquill.MappedEncoding
import ru.tinkoff.chat.entity.Provider.Provider

trait Encoders {
  implicit val providerEncoder = MappedEncoding[Provider, Int](_.id)
}
