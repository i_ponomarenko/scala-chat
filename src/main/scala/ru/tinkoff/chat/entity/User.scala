package ru.tinkoff.chat.entity

import java.time.LocalDateTime

import io.getquill.Embedded
import ru.tinkoff.chat.entity.Provider.Provider

object Provider extends Enumeration with Embedded {
  type Provider = Value
  val Telegram, TCP, WebSocket = Value
}



final case class User (
    id: Long,
    provider: Provider,
    userId: Option[Long],
    login: String,
    password: Option[String],
    isOnline: Boolean,
    registeredAt: LocalDateTime
)
