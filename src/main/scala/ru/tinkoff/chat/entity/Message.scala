package ru.tinkoff.chat.entity

import java.time.LocalDateTime


final case class Message (
 id: Long,
 userId: Long,
 text: String,
 createdAt: LocalDateTime
)
