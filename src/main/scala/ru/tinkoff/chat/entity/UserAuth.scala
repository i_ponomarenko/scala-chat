package ru.tinkoff.chat.entity

import java.time.LocalDateTime

final case class UserAuth (
  id : Long,
  userId: Long,
  isSuccess: Boolean,
  createdAt: LocalDateTime
)
