package ru.tinkoff.chat.entity

import io.getquill.MappedEncoding
import ru.tinkoff.chat.entity.Provider.Provider

trait Decoders {
  implicit val providerDecoder = MappedEncoding[Int, Provider](Provider(_))
}
