package ru.tinkoff.chat.connection

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, ActorSystem, DeadLetter, Props}
import ru.tinkoff.chat.entity.Provider
import ru.tinkoff.chat._
import akka.io.IO
import akka.io.Tcp._
import akka.util.ByteString
import org.slf4j.LoggerFactory

final case class Init()

class TCPServer(host: String, port: Int) (implicit actorSystem: ActorSystem, chat: ActorRef) extends Actor {
  val logger = LoggerFactory.getLogger(this.getClass)
  IO(akka.io.Tcp)(actorSystem)   ! Bind(self, new InetSocketAddress(host, port))

  def receive: Receive = {
    case CommandFailed(_: Bind) =>
      logger.error("Failed to start listening on " + host + ":" + port)
      context stop self
      actorSystem.terminate()
    case Bound(localAddress: InetSocketAddress) =>
      logger.info("Started listening on " + localAddress)
    case Connected(remote, local) =>
      logger.info("New tcp connection")
      sender ! Write(ByteString("Enter login\n"))
      val handler = actorSystem.actorOf(Props(classOf[TCPHandler],sender, chat, actorSystem))
      sender ! Register(handler)
      handler ! Init
  }
}

class TCPHandler(client: ActorRef, chat: ActorRef)(implicit as: ActorSystem) extends Actor {
  val logger = LoggerFactory.getLogger(this.getClass)

  def receive = waitLogin
  import as.dispatcher



  def waitLogin: Receive = {
    case Init => client ! Write(ByteString("Enter login:\n"))
    case Received(data) =>
      client ! Write(ByteString("Enter password:\n"))
      context.become(waitPass(data.utf8String))
    case PeerClosed     =>
      logger.info("TCP client has Disconnected!")
      context stop self
  }

  def waitPass(login: String): Receive = {
    case Received(data) =>
      chat ! Login(login.replace("\n", ""), None, Some(data.utf8String), Provider.TCP)
      context.become(waitAccept)
    case PeerClosed     =>
      logger.info("TCP client has Disconnected!")
      context stop self
  }

  def waitAccept: Receive = {
    case LoginSuccess =>
      client ! Write(ByteString("Successfully logged in!\n"))
      context.become(loggedIn)
    case LoginFailure =>
      context.become(waitLogin)
      client ! Write(ByteString("Failed to log in!\n"))
      self ! Init
  }

  def loggedIn: Receive = {
    case Received(data) =>
      if (data.utf8String.startsWith("/"))
        chat ! Command(data.utf8String.substring(1, data.utf8String.length - 1), None)
      else
        chat ! ChatMessage(data.utf8String, None)
    case PeerClosed     =>
      chat ! Logout(None)
      context stop self
    case ChatMessage(message, from) =>
      if (from.isEmpty)
        client ! Write(ByteString(message))
      else
        client ! Write(ByteString(s"${from.get}: $message"))
    case CommandResponse(msg) =>
      msg onComplete {
        case scala.util.Success(value) =>  client ! Write(ByteString(value))
        case scala.util.Failure(exception) => client ! Write(ByteString(exception.getMessage + "\n"))
      }
  }
}

