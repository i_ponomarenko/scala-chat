package ru.tinkoff.chat.connection

import java.math.BigInteger
import java.security.MessageDigest

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import org.slf4j.LoggerFactory
import ru.tinkoff.chat.MyJsonProtocol._
import ru.tinkoff.chat.{CheckPassword, CheckResponse}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Success

case class MyCredentials(username: String, password: String)

case class Check(userInfo: MyCredentials)

class WebSocketService(chat: ActorRef, system: ActorSystem) {
  implicit val ec = system.dispatcher

  val tokenMap: scala.collection.mutable.Map[String, MyCredentials] = scala.collection.mutable.Map.empty

  def generateToken(myCredentials: MyCredentials): String = {
    val digest = MessageDigest.getInstance("MD5").digest((myCredentials.username + myCredentials.password).getBytes())
     new BigInteger(1, digest).toString(16)
  }

  val webSocketFlows = new WebSocketFlows(chat)

  def routes: Route =
    pathEndOrSingleSlash {
      get {
        redirect("login", StatusCodes.PermanentRedirect)
      }
    } ~ path("ws" / Segment) { token =>
      println(token)
      if (tokenMap.contains(token))
        handleWebSocketMessages(webSocketFlows.webSocketFlow(tokenMap(token)))
      else
        complete(StatusCodes.Forbidden)
  } ~ path("login") {
        get {
          getFromResource("web/index.html")
        } ~ post {
          akka.http.scaladsl.server.Directives.entity(as[MyCredentials]) { user =>
            implicit val timeout: Timeout = 5.seconds
            val authenticator = system.actorOf(Props(classOf[Authenticator], chat))
            val result: Future[Boolean] = (authenticator ? Check(user)).mapTo[Boolean]
            onComplete(result) {
              case Success(value) if value =>
                val token = generateToken(user)
                tokenMap.put(token, user)
                complete(token)
              case _ =>
                complete(StatusCodes.Forbidden)
            }
          }
        } ~ options {
          respondWithHeaders(RawHeader("Access-Control-Allow-Headers", "Content-Type, Origin"),
            RawHeader("Access-Control-Allow-Origin", "*"),
            RawHeader("Access-Control-Allow-Methods", "POST")) {
            complete(StatusCodes.OK)
          }
        }
      }
}
class Authenticator(chat: ActorRef) extends Actor {
  val logger = LoggerFactory.getLogger(this.getClass)
  logger.info("New websocket authentication try")
  override def receive: Receive = {
    case Check(user) =>
      chat ! CheckPassword(user.username, user.password)
      context.become(waitResponse(sender()))
  }

  def waitResponse(replyTo: ActorRef): Receive = {
    case CheckResponse(value) => replyTo ! value
  }
}




