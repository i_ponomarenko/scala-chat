package ru.tinkoff.chat.connection

import akka.actor.ActorRef
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}
import ru.tinkoff.chat._
import ru.tinkoff.chat.entity.Provider
import spray.json._
import MyJsonProtocol._
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}



class WebSocketFlows(chat: ActorRef)(implicit ec: ExecutionContext) {
  val logger = LoggerFactory.getLogger(this.getClass)

  def chatFlow(sender: MyCredentials): Flow[String, Event, Any] = {
    val in = Flow[String].map(s => if(s.startsWith("/"))  Command(s.drop(1), Some(sender.username)) else ChatMessage(s + "\n", Some(sender.username)))
      .to(Sink.actorRef[Event](chat, Logout(Some(sender.username))))

    val out =
      Source.actorRef[Any](20, OverflowStrategy.fail).map {
        case LoginSuccess => ChatMessage("Successfully logged in", None)
        case m: ChatMessage => m
        case c: CommandResponse => c.copy(response = c.response.recover { case e: Exception => e.getMessage })

      }
        .mapMaterializedValue(ref => chat.tell(Login(sender.username, None, Some(sender.password), Provider.WebSocket), ref))

    Flow.fromSinkAndSource(in, out)
  }

  def webSocketFlow(user: MyCredentials): Flow[Message, Message, Any] =
    Flow[Message].collect {
      case TextMessage.Strict(msg) =>
        logger.debug("received message from websocket")
        msg
    }
      .via(chatFlow(user))
      .map {
        case msg: ChatMessage => TextMessage.Strict(msg.toJson.toString())
        case CommandResponse(value) =>
          TextMessage.Strict( ChatMessage(Await.result(value,5 seconds), None).toJson.toString())
      }
}

