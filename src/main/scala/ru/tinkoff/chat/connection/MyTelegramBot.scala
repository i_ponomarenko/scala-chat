package ru.tinkoff.chat.connection

import java.net.{InetSocketAddress, Proxy}

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Terminated}
import com.bot4s.telegram.api.declarative.Commands
import com.bot4s.telegram.api.{ActorBroker, AkkaDefaults, Polling, TelegramBot}
import com.bot4s.telegram.clients.ScalajHttpClient
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models.{Message, Update}
import ru.tinkoff.chat._
import ru.tinkoff.chat.entity.Provider
import slogging.{LogLevel, LoggerConfig, PrintLoggerFactory}

case class UserInfo(name: String, userId: Long)



abstract class PerChatRequests(chat: ActorRef) extends ActorBroker with AkkaDefaults {
  override val broker = Some(system.actorOf(Props(new Broker), "broker"))

  class Broker extends Actor {
    val chatActors = collection.mutable.Map[Long, ActorRef]()

    def receive = {
      case u: Update =>
        u.message.foreach { m =>
          val id = m.chat.id
          val handler = chatActors.getOrElseUpdate(m.chat.id, {
            val worker = system.actorOf(Worker.props(chat, m.source))
            context.watch(worker)
            worker ! UserInfo(m.from.get.firstName, m.from.get.id)
            worker
          })
          handler ! m
        }

      case Terminated(worker) =>
        // This should be faster
        chatActors.find(_._2 == worker).foreach {
          case (k, _) => chatActors.remove(k)
        }

      case _ =>
    }
  }

  // For every chat a new worker actor will be spawned.
  // All requests will be routed through this worker actor; allowing to maintain a per-chat state.
  class Worker(chat: ActorRef, client: Long)(implicit as: ActorSystem) extends Actor {
    import as.dispatcher
    def receive = {
      case m: Message =>
        println(m.text.get)

        if (m.text.get.startsWith("/")) {
          chat ! Command(m.text.get.drop(1), None)
        } else {
          chat ! ChatMessage(m.text.get + "\n", None)
        }
      case UserInfo(name, id) =>
        chat ! Login(name, Some(id), None, Provider.Telegram)
      case LoginSuccess =>
        request(SendMessage(client, "You are logged in"))
      case ChatMessage(message, from) =>
        if (from.isEmpty)
          request(SendMessage(client, message))
        else
          request(SendMessage(client, s"${from.get}: $message"))
      case CommandResponse(msg) =>
        msg onComplete {
          case scala.util.Success(value) =>  request(SendMessage(client, value))
          case scala.util.Failure(exception) => request(SendMessage(client, exception.getMessage + "\n"))
        }
      case _ => Unit
    }
  }

  object Worker{
    def props(chat: ActorRef, client: Long)  = Props(new Worker(chat, client))
  }
}

class MyTelegramBot(token: String)(implicit val chat: ActorRef) extends PerChatRequests(chat)
  with Polling
  with Commands
  with TelegramBot {
  LoggerConfig.factory = PrintLoggerFactory()
  val config = system.settings.config

  LoggerConfig.level = LogLevel.INFO
  val proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(config.getString("telegram.proxy.host"), config.getInt("telegram.proxy.port")))

  override val client = new ScalajHttpClient(token, proxy)

}
