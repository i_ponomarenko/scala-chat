package ru.tinkoff.chat

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import ru.tinkoff.chat.connection.{MyTelegramBot, TCPServer, WebSocketService}

import scala.io.StdIn

object Boot extends App {
  implicit val system = ActorSystem("chat")
  implicit val materializer = ActorMaterializer()
  implicit val chat = system.actorOf(Props[ChatServer], "ChatServer")

  val config = system.settings.config
  val tcp = system.actorOf(Props(classOf[TCPServer], config.getString("tcp.host"), config.getInt("tcp.port"), system, chat), "tcp")
  val telegram = new MyTelegramBot(config.getString("telegram.token")).run()
  val service = new WebSocketService(chat, system)
  val bindingFuture = Http().bindAndHandle(service.routes, config.getString("websocket.host"), config.getInt("websocket.port"))

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()

  import system.dispatcher
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
