name := "scala_chat"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies ++= Dependencies.akkaHttpStack ++ Dependencies.quill ++ Dependencies.logging ++ Dependencies.telegram
